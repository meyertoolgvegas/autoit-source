#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=Meyer Logo.ico
#AutoIt3Wrapper_Outfile=PSP Kanban.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Array.au3>
#include <Date.au3>
#include <File.au3>
#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <GuiComboBox.au3>

Global $aKits, $aPSPs, $aPlates, $sLogFile
Global $aBackends[3] = ["Kits_be.csv", "PSPs_be.csv", "Plates_be.csv"]
Global $aTabNames[3]  = ["Kits", "PSPs", "Plates"]
Global $aComboDesc[3] = ["a PSP Kit", "PSP Type", "Plate Type"]

For $i = 0 to UBound($aBackends)-1
	$aBackends[$i] = "Backend Files\" & $aBackends[$i]
Next

Global $sIniFile = "PSP Kanban.ini"

Global $Form1, $mFileMenu, $mExit, $mSettings, $mPinWindow, $MainTab
Global $idTabItem[3], $idComboDesc[3], $idComboBox[3]
Global $idQtyLabel[3], $idQtyValue[3], $idBackupLabel[1], $idBackupValue[1]
Global $idInput[3], $idUpdown[3], $idClear[3], $idAdd[3], $idRemove[3]
Global $idRadio1, $idRadio2
Global $iDelay = 50

$iX_Position = IniRead($sIniFile, "Position", "X_pos", "") ; default 563
$iY_Position = IniRead($sIniFile, "Position", "Y_pos", "") ; default 858

Func _WindowPosition()
	$aWinPos = WinGetPos($Form1)

	IniWrite($sIniFile, "Position", "X_pos", $aWinPos[0])
	IniWrite($sIniFile, "Position", "Y_pos", $aWinPos[1])

	MsgBox(0, "Success!", "Window position saved at: x-" & $aWinPos[0] & ", y-" & $aWinPos[1] & ".")
EndFunc

Func _CreateControls()
	$Form1 = GUICreate("PSP Kanban", 354, 325, $iX_Position, $iY_Position)

	$mFileMenu  = GUICtrlCreateMenu("&File")
	$mExit      = GUICtrlCreateMenuItem("&Exit", $mFileMenu)
	$mSettings  = GUICtrlCreateMenu("&Settings")
	$mPinWindow = GUICtrlCreateMenuItem("&Pin Position", $mSettings)
	$MainTab    = GUICtrlCreateTab(16, 8, 321, 280)
	GUICtrlSetFont($MainTab, 12, 400, 0, "Arial")

	For $i = 0 to 2
		$idTabItem[$i]     = GUICtrlCreateTabItem($aTabNames[$i])
		$idComboDesc[$i]   = GUICtrlCreateLabel("Select " & $aComboDesc[$i] & ":", 40, 52, 123, 24)
		$idComboBox[$i]    = GUICtrlCreateCombo("Select " & $aComboDesc[$i] & "...", 40, 76, 273, 25)
		$idQtyLabel[$i]    = GUICtrlCreateLabel("Quantity:", 40, 112, 67, 24)
		$idQtyValue[$i]    = GUICtrlCreateLabel("", 112, 112, 73, 24)

		If $i = 0 Then $idBackupLabel[$i] = GUICtrlCreateLabel("Backup:", 160, 112, 62, 24)
		If $i = 0 Then $idBackupValue[$i] = GUICtrlCreateLabel("", 232, 112, 73, 24)
		If $i = 0 Then GUICtrlSetFont($idBackupLabel[$i], 12, 400, 0, "Arial")
		If $i = 0 Then GUICtrlSetFont($idBackupValue[$i], 12, 400, 0, "Arial")
		If $i = 0 Then $idRadio1 = GUICtrlCreateRadio("Shipping Inventory", 40, 139)
		If $i = 0 Then $idRadio2 = GUICtrlCreateRadio("Backup", 160, 139)
		If $i = 0 Then GUICtrlSetState($idRadio1, $GUI_CHECKED)

		$idInput[$i]       = GUICtrlCreateInput("", 41, 168, 127, 30, BitOr($ES_CENTER,$ES_NUMBER))
		$idUpdown[$i]      = GUICtrlCreateUpdown($idInput[$i])
		$idClear[$i]       = GUICtrlCreateButton("Clear", 196, 168, 100, 30)
		$idAdd[$i]         = GUICtrlCreateButton("Add", 40, 215, 129, 49)
		$idRemove[$i]      = GUICtrlCreateButton("Remove", 184, 215, 129, 49)

		GUICtrlSetLimit($idInput[$i], 5, 0)
		GUICtrlSetLimit($idUpdown[$i], 10000, 0)

		GUICtrlSetFont($idComboDesc[$i], 12, 400, 0, "Arial")
		GUICtrlSetFont($idQtyLabel[$i],  12, 400, 0, "Arial")
		GUICtrlSetFont($idQtyValue[$i],  12, 400, 0, "Arial")
		GUICtrlSetFont($idInput[$i],     18, 400, 0, "Arial")
		GUICtrlSetFont($idAdd[$i],       22, 400, 0, "Arial")
		GUICtrlSetFont($idRemove[$i],    22, 400, 0, "Arial")
		GUICtrlSetFont($idClear[$i],     12, 400, 0, "Arial")
	Next

	GUICtrlCreateTabItem("")
EndFunc

Func _Populate_Menus($nTab, ByRef $aArray, $fRefresh=0)
	Local $sComboMenu

	_FileReadToArray($aBackends[$nTab], $aArray, $FRTA_NOCOUNT, ",")

	_GUICtrlComboBox_ResetContent($idComboBox[$nTab])
	GUICtrlSetData($idComboBox[$nTab], "Select " & $aComboDesc[$nTab] & "...")

	For $i = 1 To UBound($aArray) - 1
		Switch $nTab
			Case 0
				$sComboMenu &= $aArray[$i][0] & " - " & $aArray[$i][2] & " - " & $aArray[$i][1] & "|"
			Case 1
				$sComboMenu &= $aArray[$i][0] & " - " & $aArray[$i][1] & "|"
			Case 2
				$sComboMenu &= $aArray[$i][0] & "|"
		EndSwitch
	Next

	GUICtrlSetData($idComboBox[$nTab], $sComboMenu)
	If $fRefresh = 0 Then _GUICtrlComboBox_SetCurSel($idComboBox[$nTab], 0)
EndFunc

Func _UpdateValues($nTab, $fSleep=0)
	If $fSleep = 1 Then Sleep($iDelay)

	$nSelected = _GUICtrlComboBox_GetCurSel($idComboBox[$nTab])
	$aArray = _SwitchArrays($nTab)

	Switch $nTab
		Case 0
			$iQtyCol = 3
		Case 1
			$iQtyCol = 2
		Case 2
			$iQtyCol = 1
	EndSwitch

	If $nSelected > 0 Then
		GUICtrlSetData($idQtyValue[$nTab], $aArray[$nSelected][$iQtyCol])
		If $nTab = 0 Then GUICtrlSetData($idBackupValue[$nTab], $aArray[$nSelected][4])
	Else
		GUICtrlSetData($idQtyValue[$nTab], "")
		If $nTab = 0 Then GUICtrlSetData($idBackupValue[$nTab], "")
	EndIf
EndFunc

Func _ClearValues($nTab, $fSleep=0)
	If $fSleep = 1 Then Sleep($iDelay)

	GUICtrlSetData($idInput[$nTab], "")
	_GUICtrlComboBox_SetCurSel($idComboBox[$nTab], 0)

	_UpdateValues($nTab)
EndFunc

Func _LogChanges($nTab, $fAction, $sDescription, $nQuantity)
	Switch $nTab
		Case 0
			If GUICtrlRead($idRadio2) = $GUI_CHECKED Then
				$sType = "PSP Kit Backup"
				$nStock = GUICtrlRead($idBackupValue[$nTab]) + $nQuantity
			Else
				$sType = "PSP Kit"
				$nStock = GUICtrlRead($idQtyValue[$nTab]) + $nQuantity
			EndIf
		Case 1
			$sType = "Individual PSP"
			$nStock = GUICtrlRead($idQtyValue[$nTab]) + $nQuantity
		Case 2
			$sType = "PSP Plate"
			$nStock = GUICtrlRead($idQtyValue[$nTab]) + $nQuantity
	EndSwitch

	$sDateTime = _Now()

	$sLogString = $sDateTime & "," & $sType & ",""" & $sDescription & """," & $fAction & "," & $nQuantity & "," & $nStock & "," & "Glen B."
	FileWriteLine("PSP Kanban Log.csv", $sLogString)
EndFunc

Func _ModifyValues($nTab, $fFlag)
	$nValue = GUICtrlRead($idInput[$nTab])
	$nSelected = _GUICtrlComboBox_GetCurSel($idComboBox[$nTab])
	$aArray = _SwitchArrays($nTab)

	If $nSelected > 0 Then
		If $nValue <> 0 And $nValue > 0 Then
			If $fFlag = "Add"    Then $nValue  = $nValue
			If $fFlag = "Remove" Then $nValue *= -1

			Switch $nTab
				Case 0
					If GUICtrlRead($idRadio1) = $GUI_CHECKED Then $aArray[$nSelected][3] += $nValue
					If GUICtrlRead($idRadio2) = $GUI_CHECKED Then $aArray[$nSelected][4] += $nValue
					$sDescription = $aArray[$nSelected][0] & " - " & $aArray[$nSelected][2] & " - " & $aArray[$nSelected][1]
				Case 1
					$aArray[$nSelected][2] += $nValue
					$sDescription = $aArray[$nSelected][0] & " - " & $aArray[$nSelected][1]
				Case 2
					$aArray[$nSelected][1] += $nValue
					$sDescription = $aArray[$nSelected][0]
			EndSwitch

			_LogChanges($nTab, $fFlag, $sDescription, $nValue)
			_FileWriteFromArray($aBackends[$nTab], $aArray, Default, Default, ",")

			Switch $nTab
				Case 0
					_Populate_Menus(0, $aKits, 1)
				Case 1
					_Populate_Menus(1, $aPSPs, 1)
				Case 2
					_Populate_Menus(2, $aPlates, 1)
			EndSwitch

			_GUICtrlComboBox_SetCurSel($idComboBox[$nTab], $nSelected)
			_UpdateValues($nTab, 1)

			GUICtrlSetData($idInput[$nTab], "")
		Else
			MsgBox(0, "Error", "Please input a quantity.")
		EndIf
	Else
		MsgBox(0, "Error", "Please select " & $aComboDesc[$nTab] & ".")
	EndIf
EndFunc

Func _SwitchArrays($nTab)
	Switch $nTab
		Case 0
			Return $aKits
		Case 1
			Return $aPSPs
		Case 2
			Return $aPlates
	EndSwitch
EndFunc

Func _RefreshValues()
	_Populate_Menus(0, $aKits)
	_Populate_Menus(1, $aPSPs)
	_Populate_Menus(2, $aPlates)
EndFunc

Func _ReserveLog()
	$sLogFile = FileOpen("PSP Kanban Log.csv", 1)
	If $sLogFile = -1 Then
		MsgBox(0, "Error", "Could not reserve all dependencies. Please contact @raymond on Slack if error re-occurs.")
		Exit
	EndIf
EndFunc

_CreateControls()
_RefreshValues()
_ReserveLog()
_PSPKanban()

Func _PSPKanban()
	GUISetState(@SW_SHOW)
	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				FileClose($sLogFile)
				Exit
			Case $mExit
				FileClose($sLogFile)
				Exit
			Case $mPinWindow
				_WindowPosition()
			Case $idComboBox[0]
				_UpdateValues(0,1)
			Case $idComboBox[1]
				_UpdateValues(1,1)
			Case $idComboBox[2]
				_UpdateValues(2,1)
			Case $idClear[0]
				_ClearValues(0,1)
			Case $idClear[1]
				_ClearValues(1,1)
			Case $idClear[2]
				_ClearValues(2,1)
			Case $idAdd[0]
				_ModifyValues(0, "Add")
			Case $idAdd[1]
				_ModifyValues(1, "Add")
			Case $idAdd[2]
				_ModifyValues(2, "Add")
			Case $idRemove[0]
				_ModifyValues(0, "Remove")
			Case $idRemove[1]
				_ModifyValues(1, "Remove")
			Case $idRemove[2]
				_ModifyValues(2, "Remove")
		EndSwitch
	WEnd
EndFunc